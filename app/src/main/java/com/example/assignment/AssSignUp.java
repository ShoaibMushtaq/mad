package com.example.assignment;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.content.Intent;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.regex.Pattern;

public class AssSignUp extends AppCompatActivity {
    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[a-zA-Z])" +
                    "(?=.*[@#$%^&+=])" +
                    "(?=\\S+$)" +
                    ".{4,}" +
                    "$");
    EditText username,email,conpass,pass;
    Button signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ass_sign_up);
        signup = findViewById(R.id.button);
        username = findViewById(R.id.editTextUserName);
        email = findViewById(R.id.editTextNewEmail);
        pass = findViewById(R.id.editTextPass);
        conpass = findViewById(R.id.editTextCon);

        validateusern();
        validatemail();
        validatepas();
        validatecofpass();
        ChangeScreen();
    }
    void ChangeScreen()
    {
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent abd = new Intent (AssSignUp.this,ass3.class);
                startActivity(abd);
            }
        });
    }
    private boolean validateusern() {
        String usernameInput = username.getEditableText().toString().trim();

        if (usernameInput.isEmpty()) {
            username.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            username.setError("Username too long");
            return false;
        } else {
            username.setError(null);
            return true;
        }
    }
    private boolean validatemail() {
        String usernameInput = email.getEditableText().toString().trim();

        if (usernameInput.isEmpty()) {
            email.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            email.setError("Username too long");
            return false;
        } else {
            email.setError(null);
            return true;
        }
    }
    private boolean validatepas() {
        String passwordInput = pass.getEditableText().toString().trim();

        if (passwordInput.isEmpty()) {
            pass.setError("Field can't be empty");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            pass.setError("Password too weak");
            return false;
        } else {
            pass.setError(null);
            return true;

        }
    }
    private boolean validatecofpass() {
        String passwordInput = conpass.getEditableText().toString().trim();

        if (passwordInput.isEmpty()) {
            conpass.setError("Field can't be empty");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            conpass.setError("Password too weak");
            return false;
        } else {
            conpass.setError(null);
            return true;

        }
    }


    }

